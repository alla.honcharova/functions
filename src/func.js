const getSum = (str1, str2) => {
  if (isNaN(+str1) || typeof str1 === 'number' || typeof str1 === 'object') {
    return false
  }
  if (isNaN(+str2) || typeof str2 === 'number' || typeof str2 === 'object') {
    return false
  }
  return (+str1 + +str2).toString()
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0;
  let comments = 0;
  listOfPosts.forEach(el => {
    if (el.author === authorName) {
      posts++;
    }
    if (el.comments) {
      el.comments.forEach(comment => {
        if (comment.author === authorName) {
          comments++;
        }
      })
    }
  });
  return `Post:${posts},comments:${comments}`;
};

const tickets=(people)=> {
  let cash = 0;
    for (let i in people) {
        if (people[i] > 25 && people[i] > (cash + 25))
            return 'NO';
        cash += 25;
    }
    return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
